# Tin主题

#### Description
typecho主题模板，入手的第一个开源模板

#### 主题框架
bootstrap
#### Installation

1. 安装typecho
2. 下载主题
3. 解压到主题目录

#### Instructions

1. 后台设置即可

#### 功能介绍

1. 两栏页面
2. 暂时没有备份功能
3. 许多bug
4. 后续更新


